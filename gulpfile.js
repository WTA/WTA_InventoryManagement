'use strict';

var gulp = require('gulp'),
	jshint = require('gulp-jshint'),
	less = require('gulp-less'),
	concat = require('gulp-concat'),
	sourcemaps = require('gulp-sourcemaps'),
	uglify = require('gulp-uglify'),
	ngAnnotate = require('gulp-ng-annotate'),
	livereload = require('gulp-livereload'),
	webserver = require('gulp-webserver'),
	browserSync = require('browser-sync'),
	connect = require('gulp-connect'),
	open = require('gulp-open');

gulp.task('server', function () {
  // for more browser-sync config options: http://www.browsersync.io/docs/options/
  browserSync.init({
    // informs browser-sync to proxy our expressjs app which would run at the following location
    proxy: 'http://localhost:3000',
    // informs browser-sync to use the following port for the proxied app
    port: 9000,
    // open the proxied app in chrome

    browser: ['google-chrome']
  });
});

gulp.task('serve', function () {
	return gulp.src('dist')
		.pipe(webserver());
});

gulp.task('url', function(){
  var options = {
    url: 'http://localhost:9000'
  };
  gulp.src('./dist/index.html')
  .pipe(open('', options));
});

gulp.task('staticServer', function() {
  connect.server({
    livereload: true,
	port: 9000,
	root: ['__dirname', 'dist']
  });
});

gulp.task('jshint', function() {
	return gulp.src('src/script/**/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('js', function(){
	return gulp.src(['src/script/app.js','src/script/**/*.js'])
		.pipe(sourcemaps.init())
		.pipe(concat('app.js'))
		.pipe(ngAnnotate())
		.pipe(uglify())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('dist/'));
});

gulp.task('less', function () {
	gulp.src('src/styles/main.less') //path to your main less file
		.pipe(less())
		.pipe(gulp.dest('dist/')); // your output folder
});

gulp.task('css', function(){
	return gulp.src(['src/styles/main.css'])
		.pipe(gulp.dest('dist/'));
});

gulp.task('html', function(){
	return gulp.src(['src/views/*.html'])
		.pipe(gulp.dest('dist/views'));
});

gulp.task('index', function(){
	return gulp.src(['src/index.html'])
		.pipe(gulp.dest('dist/'));
});

gulp.task('asset', function(){
	return gulp.src(['src/asset/*.*'])
		.pipe(gulp.dest('dist/'));
});

gulp.task('watch', function(){
	livereload.listen();
	gulp.watch('src/styles/*.less',['less','css']);
	gulp.watch(['src/script/app.js','src/script/**/*.js'],['jshint','js']);
	gulp.watch(['src/views/*.html'],['html']);
	gulp.watch(['src/index.html'],['index']);
	gulp.watch(['dist/index.html','dist/app.js','dist/views/*.html','dist/main.css']).on('change', livereload.changed);
});

gulp.task('default', ['staticServer','url','js','less','css','html','asset','index','watch']);

gulp.task('local', ['server','serve','js','less','css','html','asset','index','watch']);

gulp.task('build', ['js','less','css','html','asset','index']);
