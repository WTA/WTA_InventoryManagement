'use strict';

angular.module('InventoryManager',['ngRoute', 'ngMaterial'])
.config(['$routeProvider', 
	function ($routeProvider) {
		$routeProvider.when('/dashboard', {
			templateUrl: 'views/dashboard.html'
		})
		.when('/register', {
			templateUrl: 'views/register.html'
		})
		.otherwise({
			redirectTo: '/register'
		});
	}
])
.config(['$httpProvider', function ($httpProvider) {
	// delete header from client:
	// http://stackoverflow.com/questions/17289195/angularjs-post-data-to-external-rest-api
	$httpProvider.defaults.useXDomain = true;
	delete $httpProvider.defaults.headers.common['X-Requested-With'];
}]);
