'use strict';

angular.module('InventoryManager')
.directive('register',['request', '$mdDialog',
	function (request, $mdDialog) {
	return {
		restrict: 'A',
		link: function($scope){
			$scope.cart = [];
			$scope.transaction = [];
			var _total = 0;
			$scope.addToCart = function(){

			};
			$scope.buy = function(){

			};
			$scope.return = function(){

			};
			$scope.refund = function(){

			};
			$scope.addNewProduct = function () {
				console.log($scope.newItem);
				request.addProduct($scope.newItem.description, $scope.newItem.amt, $scope.newItem.price, $scope.newItem.price).then(function (data) {
					console.log('Success!', data);
					var newProduct = {
						id: data.data,
						description: $scope.newItem.description,
						amt: $scope.newItem.amt,
						price: $scope.newItem.price
					};
					$scope.products.push(newProduct);
				}, function (data) {
					console.log('Error!', data);
				});
				$scope.showAdd();
									// console.log(transactionProducts);
							        // $scope.items = items;
			};
	        $scope.calcSub = function (product) {
	        	product.subTotal = product.qty * product.price;
	        	for (var i = 0; i < $scope.products.length; i++) {
	        		// console.log($scope.transTotal);
	        		if ($scope.transTotal) {
	        			$scope.transTotal = $scope.transTotal + $scope.products[i].subTotal;
	        		} else {
	        			$scope.transTotal = $scope.products[i].subTotal;
	        		}
	        		// console.log($scope.transTotal);
	        	}
	        };
	        $scope.closeDialog = function() {
	        	$mdDialog.hide();
			};
			$scope.showAdd = function () {
				$scope.addNew = !$scope.addNew;
			};
			$scope.addToTransaction = function (product) {
				$scope.transaction.push(product);
				// console.log($scope.transaction);
				$mdDialog.show({
					scope: $scope,
					preserveScope: true,
					template:
						'<md-dialog>' +
						'   <md-dialog-content>' +
						'	<md-list>' +
						'	  <md-subheader class="md-no-sticky">Current Shopping Cart</md-subheader>' +
						'	  <md-list-item ng-repeat="product in transaction track by $index">' +
						'	    <p> {{ product.description }} </p>' +
						'	   <md-input-container>' +
						'			<label>Quantity</label>' +
						'			<input type="number" ng-model="product.qty" ng-change="calcSub(product)">' +
						'		</md-input-container>' +
						'	   <md-input-container>' +
						'			<label>Price</label>' +
						'			<input type="number" ng-model="product.price" disabled>' +
						'		</md-input-container>' +
						'	   <md-input-container>' +
						'			<label>SubTotal</label>' +
						'			<input type="number" ng-model="product.subTotal" disabled>' +
						'		</md-input-container>' +
						'	  </md-list-item>' +
						'	  <md-list-item>' +
						'		<md-input-container style="position: relative; left: 444px;">' +
						'			<label>Total</label>' +
						'			<input type="number" ng-model="transTotal" disabled>' +
						'		</md-input-container>' +
						'	  </md-list-item>' +
						'	</md-list>' +
						'	</md-dialog-content>' +
						'	<div class="md-actions">' +
						'		<md-button ng-click="closeDialog()" class="md-primary">' +
						'			Cancel' +
						'		</md-button>' +
						'		<md-button ng-click="delete()" class="md-primary">' +
						'			Delete' +
						'		</md-button>' +
						'	</div>' +
						'</md-dialog>'
					
				});
			};
 			function init(){
				$scope.addNew = false;
				$scope.transTotal = 0;
				$scope.newItem = {
					sku: '',
					description: '',
					amt: '',
					price: ''
				};
				// request.addProduct('Computer','900','1900','400');
				request.getProducts().success(function(data){
					$scope.products = data;
					console.log(data);
				});
				request.getTransactions().success(function(data){
					$scope.transactions = data;
				});
			}
			init();

			$scope.removeProduct = function(){
				request.deleteProduct($scope.selectedId).success(function(){
					for(var i = 0, found; $scope.products.length; i++){
						found = false;
						if($scope.products[i].id === $scope.selectedId){
							$scope.products.splice(i, 1);
							found = true;
							break;
						}
					}
					if(!found){
						console.log('not found ' + $scope.selectedId + ' in ' + $scope.products);
					}
				});
				$scope.closeDialog();
			};

			$scope.deleteItem = function(prodId){
				$scope.selectedId = prodId;
				$mdDialog.show({
					scope: $scope,
					preserveScope: true,
					template:
					'<md-dialog>'+
					'	<md-subheader class="md-no-sticky">Alert</md-subheader>'+
					'		<md-dialog-content>'+
					'			<p>Are you sure you want to delete this item?</p>'+
					'			<md-button class="dialogBtnPan" ng-click="removeProduct()">Yes</md-button>'+
					'			<md-button class="closeDialogBtn" ng-click="closeDialog()">No</md-button>'+
					'		</md-dialog-content>'+
					'</md-dialog>'	
				});
			};
		}
	};
}]);
