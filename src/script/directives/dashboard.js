'use strict;'

angular.module('InventoryManager')
.directive('home',['request', function (request) {
	return {
		restrict: 'A',
		link: function(scope){

			function init(){
				scope.products = request.getProducts();
				scope.transactions = request.getTransactions();
			}
			init();
		}
	};
}]);