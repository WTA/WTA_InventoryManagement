'use strict;'

angular.module('InventoryManager')
.directive('navbar', function(){
	return {
		restrict: 'E',
		link: function(scope){
			//functions for toggling pages here
		},
		templateUrl: 'views/navbar.html'
	};
});