//Service to consolidate resource calls.
//This file is temporarily simulating a backend with the dummyData service as the db until we've done part 2
'use strict';

angular.module('InventoryManager')
.factory('request', ['$http', function ($http) {

	//INVENTORY FUNCTIONS/CALLS

	var requestFactory = {};

	requestFactory.getProducts = function(){
		return $http.get('https://wta-inventorybackend.herokuapp.com/api/product');
	};

	requestFactory.addProduct = function(description,initlAmt,price,cost){
		var product = {
			description: description,
			amt: initlAmt,
			price: price,
			cost: cost
		};
		return $http.post('https://wta-inventorybackend.herokuapp.com/api/product', product);
	};

	requestFactory.editProduct = function(prodId,product){
		return $http.put('https://wta-inventorybackend.herokuapp.com/api/product/' + prodId, product);
	};

	//TRANSACTIONS FUNCTIONS/CALLS

	requestFactory.getTransactions = function(){
		return $http.get('https://wta-inventorybackend.herokuapp.com/api/transaction');
	};

	requestFactory.getTransaction = function(transId){
		return $http.get('https://wta-inventorybackend.herokuapp.com/api/transaction/' + transId);
	};

	requestFactory.getSalesByProduct = function(prodId){
		return $http.get('https://wta-inventorybackend.herokuapp.com/api/transaction/' + prodId);
	};

	requestFactory.addTransaction = function(transaction){
		return $http.post('https://wta-inventorybackend.herokuapp.com/api/transaction',transaction);
	};

	requestFactory.editTransaction = function(id,transaction){
		return $http.put('https://wta-inventorybackend.herokuapp.com/api/transaction/' + id,transaction);
	};

	requestFactory.deleteProduct = function(prodId){
		return $http.delete('https://wta-inventorybackend.herokuapp.com/api/product/' + prodId);
	};

	return requestFactory;
}]);
